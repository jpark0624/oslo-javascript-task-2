const readline = require('readline');
const PORT = 3000;

console.log('Choose an option below:')
console.log('1. Read package.json');
console.log('2. Display OS info');
console.log('3. Start HTTP server');

const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    
    rl.question('Type a number: ', (answer) => {
        if (answer == 1){
            readPackageJson();
        } else if (answer == 2){
            displayOSInfo();
        } else if (answer == 3){
            startHTTPServer();
        } else {
            console.log('Invalid option has been chosen. Please choose between 1 to 3');
        }
        rl.close();
    });

function readPackageJson(){
    const fs = require('fs');
    console.log('Reading package.json file.');

    fs.readFile(__dirname + '/package.json', 'utf-8', (err, content) => {
        console.log(content);
    });
};

function displayOSInfo(){
    const os = require('os');
    console.log('Getting OS info...');

    let systemMemory = (os.totalmem() / 1024 / 1024 / 1024).toFixed(2);
    let freeMemory = (os.freemem() / 1024 / 1024 / 1024).toFixed(2);
    let cpuCores = os.cpus();
    let arch = os.arch();
    let platform = os.platform();
    let release = os.release();
    let user = os.userInfo().username;

    console.log(`SYSTEM MEMORY: ${systemMemory} GB`);
    console.log(`FREE MEMORY: ${freeMemory} GB`);
    console.log(`CPU CORES: ${cpuCores.length}`);
    console.log(`ARCH: ${arch}`);
    console.log(`PLATFORM: ${platform}`);
    console.log(`RELEASE: ${release}`);
    console.log(`USER: ${user}`);
};

function startHTTPServer(){
    console.log('Starting HTTP server...');

    const http = require('http');
    const server = http.createServer((req, res) => {
        try {
            if (req.url === '/'){
                res.write('Hello World!');
                res.end();
            }
        } catch (e) {
            console.log(e);
            return res.end(e.message);    
        }
    });

    server.listen(PORT, ()=>{
        console.log(`Listening on port ${PORT}...`);
    });
};
